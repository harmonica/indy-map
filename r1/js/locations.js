/*jslint indent: 2, vars: true, passfail: false */
/*globals INDY, INDYmapStyle, google, document, window, event, alert, console, setTimeout */

/**
 * Indiana Jones Travel By Map
 *
 * @module INDY
*/
var INDY = INDY || {};

/**
 * Configuration (or defaults) for the application
 * @namespace INDY
 * @class config
*/
INDY.config = {
  /**
   * Contains configuration for the journey planning form
   * @attribute form
   * @type Object
   */
  form: {
    addLocationText: '+',
    clearFieldText: 'x',
    defaultLocations: ['Edinburgh', 'Vienna', 'Delhi'],
    locationFields: [],
    removeLocationText: '-'
  }
};

/**
 * The main application code. See Google API for help: https://developers.google.com/maps/documentation/javascript/overlays
 * @namespace INDY
 * @class app
*/
INDY.app = (function () {
  "use strict";

  function Location () {

    
  }

  Location.prototype.add = function () {
    var autocomplete,
      doc = document,
      form = INDY.config.form,
      docFragment = doc.createDocumentFragment(),
      addBtn = doc.createElement('button'),
      removeBtn = doc.createElement('button'),
      clearBtn = doc.createElement('button'),
      location = doc.createElement('input'),
      label = doc.createElement('label'),
      div = doc.createElement('div'),
      journey = doc.getElementById('journey'),
      numStops = journey.querySelectorAll('input').length;

    addBtn.textContent = form.addLocationText;
    addBtn.className = 'add-location';
    removeBtn.textContent = form.removeLocationText;
    removeBtn.className = 'remove-location';
    removeBtn.onclick = this.remove();
    clearBtn.className = 'clear-field';
    clearBtn.textContent = form.clearFieldText;
    location.type = 'text';
    location.value = name || '';
    location.id = 'location' + numStops;
    label.textContent = 'Destination: ';
    label.for = location.id;
    div.id = 'container' + numStops;
    div.appendChild(label);
    div.appendChild(location);
    div.appendChild(clearBtn);
    div.appendChild(addBtn);
    div.appendChild(removeBtn);
    docFragment.appendChild(div);

    //autocomplete = new google.maps.places.Autocomplete(location);
    //autocomplete.bindTo('bounds', map);

    if (id) {
      journey.insertBefore(docFragment, doc.getElementById(id).nextSibling);  
    } else {
      journey.appendChild(docFragment);
    }
    
    doc.getElementById(div.id).querySelector('input').select();
  }

  Location.prototype.create = function () {

  }

  Location.prototype.remove = function () {
    var node = this.node;
      node.parentNode.removeChild(node);
  }

  Location.prototype.clear = function () {
    
  }

  var location = {
    /**
     * @method location.add
     * @description Add another location to the journey
     * @param {String} id The HTML id for the location to add this new one after 
     * @param {String} name The value/placeholder text for the location field. Used during setup
    */
    add: function (id, name) {
      var autocomplete,
        doc = document,
        form = INDY.config.form,
        docFragment = doc.createDocumentFragment(),
        addBtn = doc.createElement('button'),
        removeBtn = doc.createElement('button'),
        clearBtn = doc.createElement('button'),
        location = doc.createElement('input'),
        label = doc.createElement('label'),
        div = doc.createElement('div'),
        journey = doc.getElementById('journey'),
        numStops = journey.querySelectorAll('input').length;

      addBtn.textContent = form.addLocationText;
      addBtn.className = 'add-location';
      removeBtn.textContent = form.removeLocationText;
      removeBtn.className = 'remove-location';
      clearBtn.className = 'clear-field';
      clearBtn.textContent = form.clearFieldText;
      location.type = 'text';
      location.value = name || '';
      location.id = 'location' + numStops;
      label.textContent = 'Destination: ';
      label.for = location.id;
      div.id = 'container' + numStops;
      div.appendChild(label);
      div.appendChild(location);
      div.appendChild(clearBtn);
      div.appendChild(addBtn);
      div.appendChild(removeBtn);
      docFragment.appendChild(div);

      //autocomplete = new google.maps.places.Autocomplete(location);
      //autocomplete.bindTo('bounds', map);

      if (id) {
        journey.insertBefore(docFragment, doc.getElementById(id).nextSibling);  
      } else {
        journey.appendChild(docFragment);
      }
      
      doc.getElementById(div.id).querySelector('input').select();
    },

    /**
     * @method location.remove
     * @description Remove the location from the journey
     * @param {String} id The HTML id of the location to remove
    */
    remove: function (id) {
      var node = document.getElementById(id);
      node.parentNode.removeChild(node);
    },

    clearField: function (field) {
      field.value = '';
      field.select();
    } 
  };

  /**
   * @method reset
   * @description Resets the journey data back to the default settings
   * @param {String} type Soft => do not add default fields
  */
  function reset(type) {
    var config = INDY.config;
    if (type !== 'soft') {
      document.getElementById('journey').innerHTML = '';
      defaultFields();
    }
    config.gmap.coordinates = [];
    config.draw.paths = [];
  }

  /**
   * @method fireEvent
   * @description Use to fire a custom event
   * @param {String} name Name the event to be fired
   * @param {String} className The value to be used in handleEvent method
   * @param {String} target The element or object  to dispatth the event on
  */
  function fireEvent(name, className, target) {
    //http://davidwalsh.name/customevent for hints
    var myCustomEvent = new CustomEvent(name, {/*params can go here*/}) || document.createEvent("Events");
    target.className = className;
    target.dispatchEvent(myCustomEvent);
  }

  /**
   * @method defaultFields
   * @description Create some default fields for the form. Number and initial values managed in INDY.config.form.defaultLocations
  */
  function defaultFields() {
    var defaultLocations = INDY.config.form.defaultLocations;

    console.log('defaultLocations = ' + defaultLocations);

    for (var i = 0; i < defaultLocations.length; i += 1) {
      location.add(null, defaultLocations[i]);  
    }

    var input = document.getElementById('journey').querySelector('input');
    if (input.hasOwnProperty('selectionStart')) {
      input.selectionStart = 0;
      input.selectionEnd = 0;
    }
    input.focus();
  }

  /**
   * @method handleEvent
   * @param {Object} event The event that has been fired and heard
  */
  function handleEvent(event) {
    var target = event.target || null;

    console.log('action = ' + target.className);

    switch (target.className) {
    case 'add-location':
      location.add(target.parentNode.id);
      break;
    case 'build-route show-map':
      moveMap();
      // No break; since we want to run the next command
      setTimeout(buildJourney, 1000);
      break;
    case 'build-route':
      buildJourney();
      break;
    case 'route-ready':
      drawRoute();
      break;
    case 'remove-location':
      if (document.querySelectorAll('#journey input').length > 1) {
        location.remove(target.parentNode.id);
      }
      break;
    case 'another-route':
      moveMap();
      break;
    case 'clear-field':
      location.clearField(target.parentNode.getElementsByTagName('input')[0]);
      break;
    case 'reset-journey':
      clearCanvas();
      reset();
      break;
    default:
    }
  }

  /**
   * @method init
   * @description Initialise the app. Load Google Map, create default fields, attach event listeners and do some 'small device' adjusting
  */
  function init() {
    var autocomplete,
      doc = document;
    
    defaultFields(); // Setup some default fields as examples

    //
    // Setup event listeners
    //
    doc.getElementById('indy-travel').addEventListener('click', function (event) {
      event.preventDefault(); // To do: add stopPropagation as well
      handleEvent(event);
    }, false);
    
    doc.addEventListener('custom-event', function (event) {
      event.preventDefault(); // To do: add stopPropagation as well
      handleEvent(event);
    }, false);
  }

  return {
    init: init
  };
}());

document.addEventListener("DOMContentLoaded", INDY.app.init);