/*jslint indent: 2, vars: true, passfail: false */
/*globals INDY, INDYmapStyle, google, document, window, event, alert, console, setTimeout */

/**
 * Indiana Jones Travel By Map
 *
 * @module INDY
*/
var INDY = INDY || {};

/**
 * Configuration (or defaults) for the application
 * @namespace INDY
 * @class config
*/
INDY.config = {
  /**
   * Contains configuration for the Google Map initialisation
   * @attribute gmap
   * @type Object
   */
  gmap: {
    coordinates: [],
    center: new google.maps.LatLng(15, 14),
    width: 780,
    height: 480,
    streetViewControl: false,
    zoom: 2,
    style: new google.maps.StyledMapType(INDY.mapstyle, {name: "Indiana Jones"})
  },

  /**
   * Contains configuration for the drawing style of the route
   * @attribute draw
   * @type Object
   */
  draw: {
    destination: [], //Store the name of town/city we're going to
    intersectionColour: '#C00',
    paths: [],
    routeColour: '#C00',
    shadowBlur: 6,
    shadowColor: "rgba(10, 10, 10, 0.5)",
    shadowOffsetX: 1,
    shadowOffsetY: 1,
    speed: 16.67,
    stroke: 2
  },

  /**
   * Contains configuration for the journey planning form
   * @attribute form
   * @type Object
   */
  form: {
    addLocationText: '+',
    clearFieldText: 'x',
    defaultLocations: ['Edinburgh', 'Vienna', 'Delhi'],
    removeLocationText: '-'
  }
};

/**
 * The main application code. See Google API for help: https://developers.google.com/maps/documentation/javascript/overlays
 * @namespace INDY
 * @class app
*/
INDY.app = (function () {
  "use strict";

  var canvas,
    deviceType,
    map,
    overlay;

  /**
   * @method GMAPOverlay
   * @param map The Google Map that the overlay is for.
  */
  function GMAPOverlay(map) {
    this.mapOverlay = map;

    // We define a property to hold the image's div. We'll
    // actually create this div upon receipt of the onAdd()
    // method so we'll leave it null for now.
    this.divOverlay = null;
    this.setMap(map); // Call setMap on this overlay
  }

  GMAPOverlay.prototype = new google.maps.OverlayView();

  GMAPOverlay.prototype.onAdd = function () {
    // Note: an overlay's receipt of onAdd() indicates that
    // the map's panes are now available for attaching
    // the overlay to the map via the DOM.

    var div = document.createElement('div'), // Create a div to contain the canvas
      canvasElem = document.createElement('canvas'),
      gmap = INDY.config.gmap,
      panes  = this.getPanes();
    
    canvasElem.height = gmap.height;
    canvasElem.width = gmap.width;
    div.style.position = 'absolute';
    div.appendChild(canvasElem);
    this.divOverlay = div; // Set the overlay's div_ property to this DIV
    panes.overlayLayer.appendChild(div); // Add this overlay to the overlayLayer pane.
  };

  GMAPOverlay.prototype.draw = function () {
    // Size and position the overlay to cover the whole map.
    // Resize the image's DIV to fit the indicated dimensions.
    var div = this.divOverlay,
      gmap = INDY.config.gmap;
    
    canvas = overlay.divOverlay.getElementsByTagName('canvas')[0];

    div.style.left = 0;
    div.style.top = 0;
    div.style.width = gmap.width + 'px';
    div.style.height = gmap.height + 'px';
  };

  GMAPOverlay.prototype.onRemove = function () {
    this.divOverlay.parentNode.removeChild(this.divOverlay);
    this.divOverlay = null;
  };

  GMAPOverlay.prototype.alignDrawingPane = function(force) {
    window.mapProjection = this.getProjection();
    var center = window.mapProjection.fromLatLngToDivPixel(map.getCenter()),
      gmap = INDY.config.gmap,
      panes = this.getPanes();

    panes.overlayLayer.style.left = center.x - (gmap.width / 2) + 'px';
    panes.overlayLayer.style.top = center.y - (gmap.height / 2) + 'px';
  };

  /**
   * @method latLngToPixels
   * @description Convert the latitude, longitude geo co-ordinates to pixels
   * @param {Object} latlng
  */
  function latLngToPixels(latlng) {
    var overlayProjection = overlay.getProjection(),
      pixels = overlayProjection.fromLatLngToContainerPixel(latlng);
    return pixels;
  }

  /**
   * @method generateLatLng
   * @param {Number} lat Latitude
   * @param {Number} lon Longitude
   * @return geo co-ordinates of the location
  */
  function generateLatLng(lat, lon) {
    return new google.maps.LatLng(parseFloat(lat), parseFloat(lon));
  }

  /**
   * @method geoCode
   * @description Use Google Map API to geocode the location and add result to paths and geocodes arrays.
   * @param {String} location The name of the location
   * @param {Int} i The index of the location we're dealing with
  */
  function geoCode(location, i) {
    var config = INDY.config,
      geocoder = new google.maps.Geocoder(),
      paths = config.draw.paths,
      geocodes = config.gmap.coordinates;

    geocoder.geocode({address: location}, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        var center = results[0].geometry.location,
          latlng = generateLatLng(center.lat(), center.lng()),
          points = latLngToPixels(latlng);
        paths[i] = {x: points.x, y: points.y};
        geocodes[i] = latlng;
        if (paths.length === document.querySelectorAll('#journey input').length) {
          fireEvent('custom-event', 'route-ready', document); // Fire event for draw methods to start
        }
      } else {
        alert('Sorry, location "' + location + '" not found.');
      }
    });
  }

  /**
   * @method initializeGmap
   * @description Initialise the GoogleMap with custom options.
  */
  function initializeGmap() {
    var gmap = INDY.config.gmap,
      mapOptions = {
        zoom: gmap.zoom,
        center: gmap.center,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.SATELLITE, 'map_style']
        },
        streetViewControl: gmap.streetViewControl
      };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    map.mapTypes.set('map_style', gmap.style); // Associate the styled map with the MapTypeId
    map.setMapTypeId('map_style'); // Set it to display
    overlay = new GMAPOverlay(map);
  }

  //
  // Route drawing methods
  //

  /**
   * @method hypoteneuse
   * @param {Object} pathStart Reference to the starting point we will calculate the hypoteneuse from
   * @return The length of the hypoteneuse
  */
  function hypoteneuse(pathStart) {
    var paths = INDY.config.draw.paths,
      start = paths[pathStart],
      end = paths[pathStart + 1],
      zSquared = Math.pow((end.x - start.x), 2) + Math.pow((end.y - start.y), 2);
    return Math.sqrt(zSquared);
  }

  /**
   * @method capitalize
   * @description Prototype of built-in String
   * @return String with the first character in uppercase
  */
  String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  }

  /**
   * @method destinationLabel
   * @param {Object} position  Co-ordinates of destination
   * @param {Int} The leg of the overall journey we're on
  */
  function destinationLabel(position, journey) {
    var context = canvas.getContext('2d'),
      fontSize = 5.5 * map.getZoom() + 'px',
      label = INDY.config.draw.destination[journey],
      x = position.x,
      y = position.y - 6;

    context.font = 'bold ' + fontSize + ' helvetica, sans-serif ';
    context.fillStyle = '#000';
    context.shadowOffsetX = 0;
    context.shadowOffsetY = 0;
    context.shadowBlur = 0;
    context.shadowColor = '';
    context.fillText(label.capitalize(), x, y);
  }

  /**
   * @method drawCircle
   * @description Draw a circle to denote a location
   * @param {Number} x The X co-ordinate of the circle centre
   * @param {Number} y The Y co-ordinate of the circle centre
  */
  function drawCircle(x, y) {
    var config = INDY.config,
      context = canvas.getContext('2d'),
      draw = config.draw,
      radius = draw.stroke * map.getZoom() * 1.2,
      startAngle = 0,
      endAngle = 360,
      anticlockwise = false;

    context.shadowOffsetX = draw.shadowOffsetX;
    context.shadowOffsetY = draw.shadowOffsetY;
    context.shadowBlur = draw.shadowBlur;
    context.shadowColor = draw.shadowColor;
    context.arc(x, y, radius, startAngle, endAngle, anticlockwise);
    context.fillStyle = draw.intersectionColour;
    context.fill();
  }

  /**
   * @method drawLine
   * @description Draw a line between two points on the canvas
   * @param {Object} params Object to hold the arguments such as X and Y deviation, the iteration point and pathLength
  */
  function drawLine(params) {
    var draw = INDY.config.draw,
      context = canvas.getContext('2d'),
      newX = params.path.x + params.deviationX * params.iteration,
      newY = params.path.y + params.deviationY * params.iteration;

    //console.log(newX + ', ' + newY + ', ' + iteration + ', ' + journey);

    context.shadowOffsetX = draw.shadowOffsetX;
    context.shadowOffsetY = draw.shadowOffsetY;
    context.shadowBlur = draw.shadowBlur;
    context.shadowColor = draw.shadowColor;

    context.strokeStyle = draw.routeColour;
    context.beginPath();
    context.moveTo(newX - params.deviationX, newY - params.deviationY);
    context.lineWidth = (params.deviationX > 0 && params.deviationY > 0) ? (draw.stroke * map.getZoom()) - 1 : draw.stroke * map.getZoom();
    context.lineTo(newX, newY);
    context.stroke();
    context.closePath();

    if (params.iteration === params.halfwayPoint) {
      destinationLabel(params.pathEnd, params.journey + 1); // Draw town/city label
    }

    if (params.iteration >= params.pathLength) {
      console.log('End of this leg of the journey');
      setTimeout(function () {
        drawRoute(params.journey + 1);
      }, draw.speed);
    } else {
      params.iteration += 1;
      setTimeout(function () {
        drawLine(params);
      }, draw.speed);
    }
  }

  /**
   * @method drawDirectLine
   * @description Draw a line without animation. Used when zooming or moving the map
   * @param {Number} start The start point of the line
   * @param {Number} end The end point of the line
  */
  function drawDirectLine(start, end) {
    var context = canvas.getContext('2d'),
      draw = INDY.config.draw;

    context.shadowOffsetX = draw.shadowOffsetX;
    context.shadowOffsetY = draw.shadowOffsetX;
    context.shadowBlur = draw.shadowBlur;
    context.shadowColor = draw.shadowColor;
    context.strokeStyle = draw.routeColour;
    context.beginPath();
    context.moveTo(start.x, start.y);
    context.lineWidth = draw.stroke * map.getZoom();
    context.lineTo(end.x, end.y);
    context.stroke();
    context.closePath();
  }

  /**
   * @method drawRoute
   * @description Prepares the data to draw a leg of the journey
   * @param {Int} newJourney The leg of the journey we're about to draw
  */
  function drawRoute(newJourney) {
    var journey = newJourney || 0,
      paths = INDY.config.draw.paths,
      pathLength,
      start = {},
      end = {},
      params;

    if (journey >= paths.length - 1) {
      drawCircle(paths[journey].x, paths[journey].y); // End of journey so draw marker circle for city/town
      console.log('Journey ended');
      return;
    }

    start = paths[journey];
    end = paths[journey + 1];
    pathLength = hypoteneuse(journey);

    params = {
      path: start,
      pathEnd: end,
      deviationX: (end.x - start.x) / (pathLength + 4),
      deviationY: (end.y - start.y) / (pathLength + 4),
      iteration: 0,
      pathLength: pathLength,
      journey: journey,
      halfwayPoint: parseInt(pathLength * 0.75)
    };

    drawCircle(start.x, start.y);
    drawLine(params);
  }

  /**
   * @method reDrawRoute
   * @description Re-draw route after either zooming or moving the map
  */
  function reDrawRoute() {
    var coordinates = INDY.config.gmap.coordinates,
      i,
      max = coordinates.length,
      nextPoints = false,
      points,
      start,
      end;

    for (var i = 0; i < max; i += 1) {
      points = nextPoints || latLngToPixels(coordinates[i]);
      drawCircle(points.x, points.y);
      if (i < max - 1) {
        nextPoints = latLngToPixels(coordinates[i + 1]);
        start = {x: points.x, y: points.y};
        end = {x: nextPoints.x, y: nextPoints.y};
        drawDirectLine(start, end);
      }
    }
  }

  /**
   * @method buildJourney
   * @description Prepares the data for the whole journey
  */
  function buildJourney() {
    var locations = document.querySelectorAll('#journey input'),
      i,
      adjuster = 0,
      draw = INDY.config.draw,
      max = locations.length;

    if (draw.paths.length > 0) {
      clearCanvas(); // Clear existing route
      reset('soft');
    }

    for (i = 0; i < max; i += 1) {
      var destination = locations[i].value || false;

      if (destination) {
        destination = destination.split(',')[0];
        console.log('Destination = ' + destination);
        draw.destination[i] = destination;
        geoCode(locations[i].value, i - adjuster);
      } else {
        adjuster += 1;
        location.remove(locations[i].parentNode.id);
      }
    }
  }

  var location = {
    /**
     * @method location.add
     * @description Add another location to the journey
     * @param {String} id The HTML id for the location to add this new one after 
     * @param {String} name The value/placeholder text for the location field. Used during setup
    */
    add: function (id, name) {
      var autocomplete,
        doc = document,
        form = INDY.config.form,
        docFragment = doc.createDocumentFragment(),
        addBtn = doc.createElement('button'),
        removeBtn = doc.createElement('button'),
        clearBtn = doc.createElement('button'),
        location = doc.createElement('input'),
        label = doc.createElement('label'),
        div = doc.createElement('div'),
        journey = doc.getElementById('journey'),
        numStops = journey.querySelectorAll('input').length;

      addBtn.textContent = form.addLocationText;
      addBtn.className = 'add-location';
      removeBtn.textContent = form.removeLocationText;
      removeBtn.className = 'remove-location';
      clearBtn.className = 'clear-field';
      clearBtn.textContent = form.clearFieldText;
      location.type = 'text';
      location.value = name || '';
      location.id = 'location' + numStops;
      label.textContent = 'Destination: ';
      label.for = location.id;
      div.id = 'container' + numStops;
      div.appendChild(label);
      div.appendChild(location);
      div.appendChild(clearBtn);
      div.appendChild(addBtn);
      div.appendChild(removeBtn);
      docFragment.appendChild(div);

      autocomplete = new google.maps.places.Autocomplete(location);
      autocomplete.bindTo('bounds', map);

      if (id) {
        journey.insertBefore(docFragment, doc.getElementById(id).nextSibling);  
      } else {
        journey.appendChild(docFragment);
      }
      
      doc.getElementById(div.id).querySelector('input').select();
    },

    /**
     * @method location.remove
     * @description Remove the location from the journey
     * @param {String} id The HTML id of the location to remove
    */
    remove: function (id) {
      var node = document.getElementById(id);
      node.parentNode.removeChild(node);
    },

    clearField: function (field) {
      field.value = '';
      field.select();
    } 
  };

  /**
   * @method moveMap
   * @description Used by small devices to move the map on and off screen
  */
  function moveMap() {
    console.log('Moving map');
    var mapContainer = document.getElementById('map-container');
    if (mapContainer.classList.contains('small-map')) {
      mapContainer.classList.remove('small-map');
    } else {
      mapContainer.classList.add('small-map');
    }
  }

  var gmapMoved = function gmapMoved() {
    clearCanvas();
    overlay.alignDrawingPane();
    reDrawRoute();
  }

  function gmapZoomed() {
    clearCanvas();
    overlay.alignDrawingPane();
    reDrawRoute();
  }

  /**
   * @method clearCanvas
   * @description Clears the journey from the map
  */
  function clearCanvas() {
    canvas.width = canvas.width;
  }

  /**
   * @method reset
   * @description Resets the journey data back to the default settings
   * @param {String} type Soft => do not add default fields
  */
  function reset(type) {
    var config = INDY.config;
    if (type !== 'soft') {
      document.getElementById('journey').innerHTML = '';
      defaultFields();
    }
    config.gmap.coordinates = [];
    config.draw.paths = [];
  }

  /**
   * @method fireEvent
   * @description Use to fire a custom event
   * @param {String} name Name the event to be fired
   * @param {String} className The value to be used in handleEvent method
   * @param {String} target The element or object  to dispatth the event on
  */
  function fireEvent(name, className, target) {
    //http://davidwalsh.name/customevent for hints
    var myCustomEvent = new CustomEvent(name, {/*params can go here*/}) || document.createEvent("Events");
    target.className = className;
    target.dispatchEvent(myCustomEvent);
  }

  function optimalSetting() {
    var msg = document.createElement('p');
    msg.className = 'optimal-setting';
    msg.innerHTML = 'Please turn device around to portrait for it to work better.';
    document.querySelector('body').appendChild(msg);
  }

  /**
   * @method defaultFields
   * @description Create some default fields for the form. Number and initial values managed in INDY.config.form.defaultLocations
  */
  function defaultFields() {
    var defaultLocations = INDY.config.form.defaultLocations;
    for (var i = 0; i < defaultLocations.length; i += 1) {
      location.add(null, defaultLocations[i]);  
    }

    var input = document.getElementById('journey').querySelector('input');
    if (input.hasOwnProperty('selectionStart')) {
      input.selectionStart = 0;
      input.selectionEnd = 0;
    }
    input.focus();
  }

  /**
   * @method handleEvent
   * @param {Object} event The event that has been fired and heard
  */
  function handleEvent(event) {
    var target = event.target || null;

    console.log('action = ' + target.className);

    switch (target.className) {
    case 'add-location':
      location.add(target.parentNode.id);
      break;
    case 'build-route show-map':
      moveMap();
      // No break; since we want to run the next command
      setTimeout(buildJourney, 1000);
      break;
    case 'build-route':
      buildJourney();
      break;
    case 'route-ready':
      drawRoute();
      break;
    case 'remove-location':
      if (document.querySelectorAll('#journey input').length > 1) {
        location.remove(target.parentNode.id);
      }
      break;
    case 'another-route':
      moveMap();
      break;
    case 'clear-field':
      location.clearField(target.parentNode.getElementsByTagName('input')[0]);
      break;
    case 'reset-journey':
      clearCanvas();
      reset();
      break;
    default:
    }
  }

  /**
   * @method init
   * @description Initialise the app. Load Google Map, create default fields, attach event listeners and do some 'small device' adjusting
  */
  function init() {
    var autocomplete,
      doc = document,
      inputs = doc.querySelectorAll('#journey input'),
      i,
      gmap = INDY.config.gmap,
      maxInputs = inputs.length,
      mapElem = doc.getElementById('map');

    deviceType = (doc.getElementById('map').clientWidth <= 480) ? 'small' : 'large';
    doc.documentElement.classList.add(deviceType + '-device');

    if (deviceType === 'small') {
      doc.querySelector('button.build-route').classList.add('show-map');
      gmap.width = mapElem.clientWidth;
      gmap.zoom = 1;
      gmapMoved = function () {}; // Standard move adjustment not needed on iPhone, not sure about other small devices yet
    }

    initializeGmap(); // Initialise the Google Map
    defaultFields(); // Setup some default fields as examples
    optimalSetting();

    for (i = 0; i < maxInputs; i += 1) {
      autocomplete = new google.maps.places.Autocomplete(inputs[i]);
      autocomplete.bindTo(map);
    }

    //
    // Setup event listeners
    //
    doc.getElementById('indy-travel').addEventListener('click', function (event) {
      event.preventDefault(); // To do: add stopPropagation as well
      handleEvent(event);
    }, false);
    
    doc.getElementById('map-container').addEventListener('click', function (event) {
      event.preventDefault(); // To do: add stopPropagation as well
      handleEvent(event);
    }, false);
  
    doc.addEventListener('custom-event', function (event) {
      event.preventDefault(); // To do: add stopPropagation as well
      handleEvent(event);
    }, false);

    google.maps.event.addListener(map, "dragend", function () {
      console.log('Drag end');
      gmapMoved();
    });

    google.maps.event.addListener(map, "zoom_changed", function () {
      console.log('Zoom: ' + map.getZoom());
      gmapZoomed();
    });
  }

  return {
    init: init
  };
}());

document.addEventListener("DOMContentLoaded", INDY.app.init);