YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "INDY.app",
        "INDY.config"
    ],
    "modules": [
        "INDY"
    ],
    "allModules": [
        {
            "displayName": "INDY",
            "name": "INDY",
            "description": "Indiana Jones Travel By Map"
        }
    ]
} };
});